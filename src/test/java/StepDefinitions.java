import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.Assert.assertEquals;

public class StepDefinitions {

    private String today;
    private String actualAnswer;

    private static final WebDriver webDriver = new ChromeDriver();
    static {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\amidi\\IdeaProjects\\сс-scenario\\src\\test\\resources\\chromedriver.exe");
        webDriver = new ChromeDriver();
        //webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        /webDriver.manage().window().maximize();
       // chooseCityPage = new ChooseCityPage(webDriver);
        //cityMenuPage = new CityMenuPage(webDriver);
    }

    @Given("today is {string}")
    public void today_is(String today) {
        this.today = today;
        // Write code here that turns the phrase above into concrete actions
        //throw new io.cucumber.java.PendingException();
    }
    @When("I ask about whether it's Friday yet")
    public void i_ask_about_whether_it_s_friday_yet() {
        this.actualAnswer = WeekAnaluser.isItFridayToday(today);
    }
    @Then("I should be told {string}")
    public void i_should_be_told(String string) {
    assertEquals("Nope", actualAnswer);
    }

}
